export class User {
  id: number;
  username: string;
  password: string;
  email: string;
  token: string;
}

export interface UserUpdate {
  email: string;
  username: string;
  password: string;
}
