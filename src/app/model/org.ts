export class Organisation {
  id: number;
  title: string;
}

export interface OrganisationUpdate {
  title: string;
}
