import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from '../../services/login.service';
import { OrganisationService } from '../../services/organisation.service';
import { OrganisationResponse } from '../types';

@Component({
  selector: 'app-new-organisation',
  templateUrl: './new-organisation.component.html',
  styleUrls: ['./new-organisation.component.scss']
})
export class NewOrganisationComponent {
  organisationForm: FormGroup;
  added: OrganisationResponse;

  constructor(
    private organisationService: OrganisationService,
    private authentication: LoginService
  ) {
    this.organisationForm = new FormGroup({
      title: new FormControl('', Validators.required)
    });
  }

  submit() {
    this.organisationService.newOrganisation(this.organisationForm.value).subscribe(answer => {
      console.log('answer', answer);
      this.added = answer;
      this.organisationForm.reset();
    });
  }
}
