import { Component, OnInit } from '@angular/core';

import { OrganisationService } from '../../services/organisation.service';
import { OrganisationResponse} from '../types';


@Component({
  selector: 'app-list-organisation',
  templateUrl: './list-organisation.component.html',
  styleUrls: ['./list-organisation.component.scss']
})
export class ListOrganisationComponent implements OnInit {
  organisation: OrganisationResponse;


  constructor(private organisationService: OrganisationService) {
    this.organisationService.getCurrentOrganisation();
  }

  ngOnInit() {
    this.organisationService.getCurrentOrganisation();
  }
}
