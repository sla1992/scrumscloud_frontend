export interface NewOrganisation {
    title: string;
}

export interface OrganisationResponse {
    id: number;
    title: string;
}

export type OrganisationListResponse = OrganisationResponse[];
