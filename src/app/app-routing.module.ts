import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { OrganisationComponent } from './organisation/organisation.component';
import { NewOrganisationComponent } from './organisation/new/new-organisation.component';
import { ListOrganisationComponent } from './organisation/list/list-organisation.component';
import { ProfileComponent } from './profile/profile.component';
import { FileviewComponent } from './fileview/fileview.component';
import { LogoutComponent } from './logout/logout.component';
import {AuthGuard} from './handlers/auth-guard';
import {MembersComponent} from './organisation/members/members.component';

const routes: Routes = [
  {
    path: '',
    component: WelcomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'logout',
    component: LogoutComponent,
  },

  {
    path: 'org',
    component: OrganisationComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'new',
        component: NewOrganisationComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'list',
        component: ListOrganisationComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'members',
        component: MembersComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthGuard]
      },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
