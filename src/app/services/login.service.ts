/* tslint:disable:no-trailing-whitespace */
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

import {environment} from '../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {User, UserUpdate} from '../model/user';
import {map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  private auth: any;

  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(email: string, password: string) {
    return this.http.post<User>(environment.api + '/users/login/', {email, password})
      .pipe(map(user => {
        if (user && user.token) {
          localStorage.setItem('currentUser', JSON.stringify(user));
          localStorage.setItem('token', user.token);

          this.currentUserSubject.next(user);
        }
        return user;
      }));

  }

  updateProfile(userUpdate: UserUpdate) {
    return this.http.put<User>(environment.api + '/users/', userUpdate)
      .pipe(map(user => {
        console.log('UPDATED USER?');
        this.currentUserSubject.next(user);
        return user;
      }));

  }

  logout() {
    localStorage.clear();
    this.auth.logout();
  }

  isLoggedIn() {
    if (window.localStorage.getItem('token')) {
      return true;
    }
    return false;
  }
}
