import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {LoginService} from './login.service';
import { OrganisationResponse, OrganisationListResponse, NewOrganisation } from '../organisation/types';
import { environment } from '../../environments/environment';
import { Organisation } from '../model/org';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class OrganisationService {
  private currentOrganisationSubject: BehaviorSubject<Organisation>;
  public currentOrganisation: Observable<Organisation>;

  private org: Organisation;

  constructor(private http: HttpClient,
              private router: Router,
              private authentication: LoginService) {
    this.currentOrganisationSubject = new BehaviorSubject<Organisation>(JSON.parse(localStorage.getItem('currentOrganisation')));
    this.currentOrganisation = this.currentOrganisationSubject.asObservable();
  }

  newOrganisation(organisation: NewOrganisation) {
    return this.http
      .post<OrganisationResponse>(environment.api + `/org`, organisation);
  }

  getCurrentOrganisation() {
    return this.http
      .get<OrganisationResponse>(environment.api + `/org/${this.authentication.currentUserValue.id}`)
      .pipe(map(org => {
        if (org) {
          localStorage.setItem('currentOrganisation', JSON.stringify(org));
          return this.currentOrganisationSubject.next(org);
        }
        return this.org;
      }));
  }

  getOrganisation() {
    return this.http
      .get<OrganisationListResponse>(environment.api + `/org`);
  }
}
