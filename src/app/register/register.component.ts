import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../services/register.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AlertService} from '../services/alert.service';
import { LoginService } from '../services/login.service';
import {first, map} from 'rxjs/operators';


@Component({
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(private registerService: RegisterService,
              private formBuilder: FormBuilder,
              private router: Router,
              private alertService: AlertService,
              private authenticationService: LoginService,
              ) {
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
      // tslint:disable-next-line:max-line-length
      password: ['', [Validators.required,  Validators.minLength(12), Validators.maxLength(40), Validators.pattern('(?=.*[0-9])(?=.*[!@#$%^&*,.-/()=+])[a-zA-Z0-9!@#$%^&*,.-/()=+]+$')]],
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    this.registerService.register(this.registerForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Registration successful', true);
          this.router.navigate(['/login']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
