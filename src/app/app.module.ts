import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule} from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { RegisterComponent } from './register/register.component';
import { OrganisationComponent } from './organisation/organisation.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NewOrganisationComponent } from './organisation/new/new-organisation.component';
import { ListOrganisationComponent } from './organisation/list/list-organisation.component';
import { AuthErrorHandler} from './handlers/auth-error-handler';
import { TokenInterceptor } from './handlers/auth-token-handler';
import { LogoutComponent } from './logout/logout.component';
import { FileviewComponent } from './fileview/fileview.component';
import { ProfileComponent } from './profile/profile.component';
import { AlertComponent} from './components/alert.component';
import { MembersComponent } from './organisation/members/members.component';


@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    WelcomeComponent,
    RegisterComponent,
    OrganisationComponent,
    SidebarComponent,
    NewOrganisationComponent,
    ListOrganisationComponent,
    LogoutComponent,
    FileviewComponent,
    ProfileComponent,
    MembersComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule, ReactiveFormsModule],

  providers: [
    {
      provide: ErrorHandler,
      useClass: AuthErrorHandler
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
